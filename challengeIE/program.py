#!/usr/bin/env python3

import sys
import itertools

# Function to generate strings
def findStrings(n, k):
    # Find all combinations with positions for k ones
    allPositions = itertools.combinations(range(n), k)
    strings = []
    # For each set of positions, create string with ones in those positions
    for positions in allPositions:
        string = ['0'] * n
        for position in positions:
            string[position] = '1'
        strings.append(''.join(string))
    # Return results
    return strings

# Function to display results
def dispResult(strings):
    for string in sorted(strings):
        print(string)

# Main function to read input, find strings, and display result
if __name__ == "__main__":
    n = 0
    for line in sys.stdin:
        if n:
            print()
        n, k = [int(x) for x in line.split()]
        dispResult(findStrings(n, k))
