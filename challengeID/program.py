#!/usr/bin/env python3

import sys

# functions
def canPlant(midpoint, spots, length, flowers):
    position = spots[0]
    count = 1

    for i in range(length):
        if (spots[i] - position) >= midpoint:
            position = spots[i]
            count += 1

            if count == flowers:
                return True

    return False

def maxLength(spots, length, flowers):
    dist = -1
    left = 0
    right = spots[length-1]-spots[0]+1

    while (left < right):
        midpoint = (left + right)//2

        if (canPlant(midpoint, spots, length, flowers)):
            dist = max(dist, midpoint)
            left = midpoint+1
        else:
            right = midpoint
    
    return dist-1

# exec
if __name__ == '__main__':
    while True:
        Numbers = [int(x) for x in sys.stdin.readline().strip().split()]

        if not Numbers:
            exit()

        pots = Numbers[0]
        flowers = Numbers[1]
        potSpots = []

        for i in range(pots):
            line = sys.stdin.readline()
            potSpots.append(int(line.strip()))

        potSpots.sort()
        length = len(potSpots) 
        print(maxLength(potSpots, length, flowers))
