#!/usr/bin/env python3

import sys

# Find maximum number of views
def findViews(buildings):
    maxHeight = 0
    totalViews = 0
    # Check each building for view
    while buildings:
        height = buildings.pop()
        if height > maxHeight:
            totalViews += 1
            maxHeight = height
    return totalViews 

# Read input and print results
if __name__ == "__main__":
    for line in sys.stdin:
        buildings = [int(x) for x in line.split()]
        print(findViews(buildings))
