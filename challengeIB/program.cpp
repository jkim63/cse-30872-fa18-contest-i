#include <iostream>
#include <string.h>
#include<bits/stdc++.h> 

using namespace std;

const size_t NLETTERS = 256;

bool isomorphic(string s, string t) {
    int s1 = s.length();
    int s2 = t.length();

    if (s1 != s2) 
       return false;

    //compute histogram
    bool seen[NLETTERS] = {false};

    int map[NLETTERS];
    memset(map, -1, sizeof(map));

    for (int i = 0; i < s2; i++) {     // check
        if (map[s[i]] == -1) {
            if (seen[t[i]] == true)
                return false;

            seen[t[i]] = true;
            map[s[i]] = t[i];
        } else if (map[s[i]] != t[i])
            return false;
    }

    return true;
}

int main(int argc, char *argv[]) {
    // Take input
    string s1, s2;
    int count = 0;

    while (cin >> s1 >> s2) {
        if (isomorphic(s1, s2)){
           cout << "Isomorphic" << endl; 
        } else {
            cout << "Not Isomorphic" << endl;
        }
    }
    
    return 0;
}

