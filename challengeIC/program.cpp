#include <iostream>
#include <algorithm>
#include <vector>


using namespace std;

int main(int argc, char * argv[]) {
    int N, k;
    while(cin >> N >> k) {
        if (N == 0)
            continue;
        vector<int> vals(N);
        for(int i = 0; i < N; i++) {
            cin >> vals[i];
        }
        for(int i = 0, j =0; j < k && i < N; i++, j++) {
            vector<int>::iterator it = max_element(vals.begin()+i, vals.end());
            int maxv = *it;
            if(maxv == vals[i]) {
                j--;
                continue;
            }
            *it = vals[i];
            vals[i] = maxv;
        }
        cout << vals[0];
        for (int i = 1; i < N; i++) {
            cout << " " << vals[i];
        }
        cout << endl;
    }


}