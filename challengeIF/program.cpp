#include <iostream>
#include <vector>

using namespace std;

vector<vector<vector<int>>> pos;
int maxc = 0;
int limit = 101;



void ways(int score) {
    if (score > maxc) {
        for( int s = maxc; s <= score; s++, maxc++) {
            if(s + 2 < limit) {
                for(int i = 0; i < (int)pos[s].size(); i++) {
                    vector<int> v;
                    for(int j = 0; j < (int)pos[s][i].size(); j++) {
                        v.push_back(pos[s][i][j]);
                    }
                    v.push_back(2);
                    pos[s+2].push_back(v);
                }
            }
            if(s + 3 < limit) {
                for(int i = 0; i < (int)pos[s].size(); i++) {
                    if(pos[s][i][pos[s][i].size()-1] < 3)
                        continue;
                    vector<int> v;
                    for(int j = 0; j < (int)pos[s][i].size(); j++) {
                        v.push_back(pos[s][i][j]);
                    }
                    v.push_back(3);
                    pos[s+3].push_back(v);
                }
            }
            if(s + 7 < limit) {
                for(int i = 0; i <(int)pos[s].size(); i++) {
                    if(pos[s][i][pos[s][i].size()-1] < 7)
                        continue;
                    vector<int> v;
                    for(int j = 0; j < (int)pos[s][i].size(); j++) {
                        v.push_back(pos[s][i][j]);
                    }
                    v.push_back(7);
                    pos[s+7].push_back(v);
                }
            }
        }
    }
    int num = pos[score].size();
    cout << "There " << ( (num != 1) ? "are " : "is " ) << num << ( (num != 1) ? " ways": " way") << " to achieve a score of " << score << ":" << endl;
    for (int i = num-1; i >= 0; i--) {
        for(int j = (int)pos[score][i].size()-1;j>=0; j--) {
            cout << pos[score][i][j];
            if(j-1 >= 0)
                cout << " ";
        }
        cout << endl;
    }
}


int main(int argc, char* argv[]) {
    pos = vector<vector<vector<int>>> (limit);
    vector<int> v;
    v.clear();
    v.push_back(2);
    pos[2].push_back(v);
    v.clear();
    v.push_back(3);
    pos[3].push_back(v);
    v.clear();
    v.push_back(7);
    pos[7].push_back(v);
    int score;
    while(cin >> score) {
        ways(score);
    }


    return 0;
}
